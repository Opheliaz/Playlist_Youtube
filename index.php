<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title> YOU MIX </title>
        <link rel="stylesheet" type="text/css" href="index.css"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="shortcut icon" href="ici le nom de ton favicon.ico">
        <!-- <link rel="icon" type="images/tete.png" href="tete.png" /> -->
    </head>
    <body>
        <header>
            <div class = "titre">
                <img src = "images/tete.png" alt="">
                <h1> YOU MIX </h1>
            </div>
        </header>
        <!-- Premier formulaire pour insérer le lien video youtube -->
            <div class = "form">
                <form action="forms/form1.php" method="POST" id="form3"><br>
                <input class="form1" type="textarea" name="url"><br>
                <input class="ajouter" type="image"src="images/add1.png" name="save" value="submit"/>
                </form>
            </div>

        <?php
        include_once ('config.php');

        $bdd = connect();
        $statement = $bdd->query('SELECT * FROM video order by id desc');
        $videos = $statement->fetchAll();

        // si on a au moins 1 video
        if (count($videos) > 0) {
            if (array_key_exists('idurl', $_POST)) {
                $idUrl = $_POST['idurl'];
            } else {
                $idUrl = explode("v=", $videos[0]['url'])[1];
            }

            ?>
            <div class='globale'>
                <div class= "video">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $idUrl; ?>" frameborder="0" gesture="media" allow="encrypted-media"allowfullscreen></iframe>
                </div>
                <div class="lists">
        <?php
        }

        else {
            echo 'il n\'y pas encore de videos';
        }

        foreach($videos as $video){
            $idUrl = explode("v=", $video['url'])[1];
        ?>
        <!-- formulaire pour lancer les liens video youtube -->
        <div class= "button">
            <form action="index.php" method="POST" id="form1">
                <input class="video1" type="text" name="url" value="<?php echo $video['url']; ?>" />
                <input type="hidden" name="idurl" value="<?php echo $idUrl; ?>"/>
                <input class ="playing" type="image" src ="images/play.png" name="play" value="Lecture" width ="19px" height ="19px"/>
            </form>

        <!-- formulaire pour supprimer les lien video youtube -->
            <form action="forms/form2.php" method="POST" id="form2">
                <input class ="dead" type="image" src="images/delete2.png" name="suppr" value="submit" width ="23px" height = "20px"/>
                <input type="hidden" name="idurl" value="<?php echo $video['id']; ?>"/>
            </form>
        </div>
<?php
   }
    ?>
    </div>
    </div>
    </body>
    </html>